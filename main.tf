provider "aws" {
}
variable "vpc-cidr-block" {
    description = "this is the cidr block for the new vpc"
  
}

resource "aws_vpc" "dev-vpc" {
    cidr_block = var.vpc-cidr-block
    tags = {
        Name = "vpc demo"
      
    }
}

resource "aws_subnet" "dev-sub-1" {
    vpc_id = data.aws_vpc.default_vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = "eu-north-1a"
    tags = {
        Name = "subnet demo"
    }
  
}
data "aws_vpc" "default_vpc" {
    default = true
  
}
output "vpc_id" {
    value = "aws_vpc.default_vpc.id"
}
output "subnet_id" {
    value = "aws_subnet.default_vpc.id"
}